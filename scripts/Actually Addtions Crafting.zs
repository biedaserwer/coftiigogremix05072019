//Machine Recipe Changes

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered>, 100000, 650, <actuallyadditions:item_crystal>, [<ore:dyeRed>, <minecraft:redstone>, <ore:ingotBrick>, <minecraft:netherbrick>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:block_crystal_empowered>, 250000, 650, <actuallyadditions:block_crystal>, [<ore:dyeRed>, <minecraft:redstone>, <ore:ingotBrick>, <minecraft:netherbrick>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered:1>, 100000, 650, <actuallyadditions:item_crystal:1>, [<ore:dyeCyan>, <minecraft:prismarine_shard>, <minecraft:prismarine_shard>, <minecraft:prismarine_shard>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:block_crystal_empowered:1>, 250000, 650, <actuallyadditions:block_crystal:1>, [<ore:dyeCyan>, <minecraft:prismarine_shard>, <minecraft:prismarine_shard>, <minecraft:prismarine_shard>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered:2>, 100000, 650, <actuallyadditions:item_crystal:2>, [<ore:dyeLightBlue>, <minecraft:clay_ball>, <minecraft:clay_ball>, <minecraft:clay>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:block_crystal_empowered:2>, 250000, 650, <actuallyadditions:block_crystal:2>, [<ore:dyeLightBlue>, <minecraft:clay_ball>, <minecraft:clay_ball>, <minecraft:clay>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered:5>, 100000, 650, <actuallyadditions:item_crystal:5>, [<ore:dyeGray>, <minecraft:snowball>, <minecraft:stone_button>, <minecraft:cobblestone>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:block_crystal_empowered:5>, 250000, 650, <actuallyadditions:block_crystal:5>, [<ore:dyeGray>, <minecraft:snowball>, <minecraft:stone_button>, <minecraft:cobblestone>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered:3>, 100000, 650, <actuallyadditions:item_crystal:3>, [<ore:dyeBlack>, <ore:crystalSlag>, <ore:crystalSlag>, <minecraft:stone>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:block_crystal_empowered:3>, 250000, 650, <actuallyadditions:block_crystal:3>, [<ore:dyeBlack>, <ore:crystalSlag>, <ore:crystalSlag>, <minecraft:stone>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_crystal_empowered:4>, 100000, 650, <actuallyadditions:item_crystal:4>, [<ore:dyeLime>, <ore:itemFlower>, <ore:treeSapling>, <minecraft:slime_ball>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<actuallyadditions:item_misc:24>, 100000, 650, <actuallyadditions:item_misc:23>, [<actuallyadditions:item_canola_seed>, <actuallyadditions:item_canola_seed>, <actuallyadditions:item_canola_seed>, <actuallyadditions:item_canola_seed>]);

