mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_sword>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_bow>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_pickaxe>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_shovel>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_axe>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_hoe>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_helmet>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_chestplate>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_pants>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_boots>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:endest_pearl>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:ultimate_stew>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:neutron_collector>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:resource:5>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:resource:6>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:neutronium_compressor>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:skullfire_sword>);


mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:hoverboard_cell>, [
	[null, null, <variedcommodities:ingot_mithril>, null, null], 
	[null, <variedcommodities:ingot_mithril>, <thermalfoundation:material:515>, <variedcommodities:ingot_mithril>, null], 
	[null, <variedcommodities:ingot_mithril>, <immersiveengineering:metal_decoration0:1>, <variedcommodities:ingot_mithril>, null], 
	[null, <variedcommodities:ingot_mithril>, <thermalfoundation:material:515>, <variedcommodities:ingot_mithril>, null], 
	[null, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:hoverboard_capacitor>, [
	[null, <ironjetpacks:hoverboard_cell>, null, <ironjetpacks:hoverboard_cell>, null], 
	[<variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>], 
	[<variedcommodities:ingot_mithril>, <ore:dustRedstone>, <fluxnetworks:fluxpoint>, <ore:dustRedstone>, <variedcommodities:ingot_mithril>], 
	[<variedcommodities:ingot_mithril>, <ore:dustRedstone>, <fluxnetworks:fluxpoint>, <ore:dustRedstone>, <variedcommodities:ingot_mithril>], 
	[<variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:hoverboard_thruster>, [
	[null, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, <variedcommodities:ingot_mithril>, null], 
	[<variedcommodities:ingot_mithril>, <ore:dustRedstone>, <ore:blockMagma>, <ore:dustRedstone>, <variedcommodities:ingot_mithril>], 
	[<variedcommodities:ingot_mithril>, <ore:blockMagma>, <car:engine_3_cylinder>, <ore:blockMagma>, <variedcommodities:ingot_mithril>], 
	[<variedcommodities:ingot_mithril>, <mekanismgenerators:turbineblade>, <thermalfoundation:material:515>, <mekanismgenerators:turbineblade>, <variedcommodities:ingot_mithril>], 
	[<randomthings:superlubricentplatform>, <cyclicmagic:plate_vector>, <randomthings:superlubricentplatform>, <cyclicmagic:plate_vector>, <randomthings:superlubricentplatform>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hoverboard:hoverboard>, [
	[null, null, null, null, null], 
	[<ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_capacitor>, <actuallyadditions:item_suction_ring>, <ironjetpacks:hoverboard_capacitor>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>], 
	[null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hoverboard:hoverboard>, [
	[<ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_capacitor>, <actuallyadditions:item_suction_ring>, <ironjetpacks:hoverboard_capacitor>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>], 
	[null, null, null, null, null], 
	[null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hoverboard:hoverboard>, [
	[null, null, null, null, null], 
	[null, null, null, null, null], 
	[<ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_capacitor>, <actuallyadditions:item_suction_ring>, <ironjetpacks:hoverboard_capacitor>, <ore:plateAluminum>], 
	[<ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>, <ironjetpacks:hoverboard_thruster>, <ore:plateAluminum>]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <tconstruct:materials:50>, [
	[<extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:singularity:26>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:singularity:26>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:singularity:26>, <extendedcrafting:singularity:26>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:singularity:26>, <extendedcrafting:singularity:26>, <extendedcrafting:singularity:26>, <extendedcrafting:singularity:26>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <ore:blockElectrum>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>, <extendedcrafting:storage:4>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <yoyos:creative_yoyo>.withTag({ENABLED: 1 as byte, RARITY: 4}), [
	[null, null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null, null, null], 
	[null, <ore:blockInfinity>, <extendedcrafting:singularity:6>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null, null], 
	[<ore:blockInfinity>, <extendedcrafting:singularity:6>, <ore:blockInfinity>, <extendedcrafting:singularity:48>, <extendedcrafting:singularity:48>, <avaritia:resource:5>, <ore:blockInfinity>, null, null], 
	[<ore:blockInfinity>, <ore:blockInfinity>, <extendedcrafting:singularity:48>, <extendedcrafting:singularity:48>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <ore:blockInfinity>, null], 
	[<ore:blockInfinity>, <ore:blockInfinity>, <extendedcrafting:singularity:48>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <extendedcrafting:singularity:2>, <ore:blockInfinity>, <ore:ingotCosmicNeutronium>], 
	[null, <ore:blockInfinity>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <extendedcrafting:singularity:2>, <extendedcrafting:singularity:2>, <ore:blockInfinity>, <ore:ingotCosmicNeutronium>], 
	[null, null, <ore:blockInfinity>, <avaritia:resource:5>, <extendedcrafting:singularity:2>, <extendedcrafting:singularity:2>, <ore:blockInfinity>, null, <ore:ingotCosmicNeutronium>], 
	[null, null, null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null, <ore:ingotCosmicNeutronium>], 
	[null, null, null, null, null, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, null]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <extrautils2:creativeenergy>, [
	[null, null, null, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, null, null, null], 
	[null, null, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, null, null], 
	[null, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, null], 
	[<extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>], 
	[<extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <tradeasteroid:alloyblock2>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>], 
	[null, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, null], 
	[null, null, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, <thermalexpansion:capacitor:4>, <extendedcrafting:storage:4>, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <galacticraftcore:infinite_oxygen>, [
	[null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null], 
	[<ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>], 
	[<ore:blockUltimate>, <mekanism:gasmask>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gasmask>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <mekanism:gastank>, <galacticraftcore:sealer>, <mekanism:scubatank>, <mekanism:scubatank>, <mekanism:scubatank>, <galacticraftcore:sealer>, <mekanism:gastank>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <mekanism:gastank>, <mekanism:scubatank>, <galacticraftcore:oxygen_gear>, <galacticraftcore:oxygen_gear>, <galacticraftcore:oxygen_gear>, <mekanism:scubatank>, <mekanism:gastank>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <mekanism:gastank>, <mekanism:scubatank>, <galacticraftcore:oxygen_gear>, <galacticraftcore:oxygen_gear>, <galacticraftcore:oxygen_gear>, <mekanism:scubatank>, <mekanism:gastank>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <mekanism:gastank>, <galacticraftcore:sealer>, <mekanism:scubatank>, <mekanism:scubatank>, <mekanism:scubatank>, <galacticraftcore:sealer>, <mekanism:gastank>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <mekanism:gasmask>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gastank>, <mekanism:gasmask>, <ore:blockUltimate>], 
	[<galacticraftcore:oxygen_gear>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <galacticraftcore:oxygen_gear>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_sword>, [
	[null, null, null, null, null, null, null, <defier:energystar:2>, <defier:energystar:2>], 
	[null, null, null, null, null, null, <defier:energystar:2>, <defier:energystar:2>, <defier:energystar:2>], 
	[null, null, null, null, null, <armorplus:ender_dragon_sword>, <defier:energystar:2>, <defier:energystar:2>, null], 
	[null, null, null, null, <armorplus:infused_lava_sword>, <defier:energystar:2>, <armorplus:ender_dragon_sword>, null, null], 
	[null, <ore:blockUltimate>, <ore:blockUltimate>, <armorplus:guardian_sword>, <defier:energystar:2>, <armorplus:infused_lava_sword>, null, null, null], 
	[null, null, <ore:blockUltimate>, <draconicevolution:draconic_sword>, <armorplus:guardian_sword>, null, null, null, null], 
	[null, <ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockUltimate>, <ore:blockUltimate>, null, null, null, null], 
	[<ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>, null, <ore:blockUltimate>, null, null, null, null], 
	[<ore:blockInfinity>, <ore:blockInfinity>, null, null, null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_helmet>, [
	[null, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, null], 
	[<ore:blockInfinity>, <ore:blockUltimate>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <ore:blockUltimate>, <ore:blockInfinity>], 
	[<ore:blockInfinity>, <ore:blockUltimate>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <armorplus:infused_lava_helmet>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockUltimate>, <ore:blockInfinity>], 
	[<ore:blockInfinity>, <ore:blockUltimate>, <avaritia:resource:5>, <armorplus:super_star_helmet>, <draconicevolution:draconic_chest>, <armorplus:ender_dragon_helmet>, <avaritia:resource:5>, <ore:blockUltimate>, <ore:blockInfinity>], 
	[<ore:blockInfinity>, <ore:blockUltimate>, null, null, null, null, null, <ore:blockUltimate>, <ore:blockInfinity>], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_chestplate>, [
	[null, <ore:blockUltimate>, <ore:blockInfinity>, <avaritia:resource:5>, <avaritia:resource:5>, <avaritia:resource:5>, <ore:blockInfinity>, <ore:blockUltimate>, null], 
	[<ore:blockUltimate>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockUltimate>], 
	[<ore:blockInfinity>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockInfinity>], 
	[<ore:blockInfinity>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockInfinity>], 
	[<ore:blockUltimate>, <ore:blockInfinity>, <ore:blockInfinity>, <armorplus:guardian_chestplate>, <draconicevolution:draconic_chest>, <armorplus:super_star_chestplate>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockUltimate>], 
	[null, null, <ore:blockInfinity>, <ore:blockCosmicNeutronium>, <armorplus:infused_lava_chestplate>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>, null, null], 
	[null, null, <ore:blockInfinity>, <tradeasteroid:alloyblock2>, <avaritia:resource:5>, <tradeasteroid:alloyblock2>, <ore:blockInfinity>, null, null], 
	[null, null, <ore:blockInfinity>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockInfinity>, null, null], 
	[null, null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_pants>, [
	[<ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <armorplus:super_star_leggings>, <armorplus:infused_lava_leggings>, <armorplus:ender_dragon_leggings>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_legs>, <ore:blockCosmicNeutronium>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>], 
	[<ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null, null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>], 
	[<ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, null, null, null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, null, null, null, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, null, null, null, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>], 
	[<ore:blockUltimate>, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, null, null, null, <tradeasteroid:alloyblock2>, <tradeasteroid:alloyblock2>, <ore:blockUltimate>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_boots>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, <draconicevolution:draconic_boots>, <armorplus:super_star_boots>, <ore:blockUltimate>, null, <ore:blockUltimate>, <armorplus:ender_dragon_boots>, <armorplus:infused_lava_boots>, null], 
	[null, <ore:blockInfinity>, <ore:blockNetherStar>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockNetherStar>, <ore:blockInfinity>, null], 
	[null, <ore:blockInfinity>, <ore:blockNetherStar>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockNetherStar>, <ore:blockInfinity>, null], 
	[null, <ore:blockInfinity>, <ore:blockNetherStar>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockNetherStar>, <ore:blockInfinity>, null], 
	[null, <ore:blockInfinity>, <ore:blockNetherStar>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockNetherStar>, <ore:blockInfinity>, null], 
	[null, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockInfinity>, <ore:blockInfinity>, null], 
	[<ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>], 
	[<ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockUltimate>, null, <ore:blockUltimate>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_bow>, [
	[null, null, null, null, <ore:blockUltimate>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>, <ore:blockInfinity>], 
	[null, null, null, <ore:blockUltimate>, <ore:blockUltimate>, null, null, null, <avaritia:resource:5>], 
	[null, null, <defier:energystar:2>, <defier:energystar:2>, null, null, null, <avaritia:resource:5>, null], 
	[null, <ore:blockUltimate>, <defier:energystar:2>, null, null, null, <avaritia:resource:5>, null, null], 
	[<ore:blockUltimate>, <ore:blockUltimate>, null, null, null, <avaritia:resource:5>, null, null, null], 
	[<ore:blockInfinity>, null, null, null, <avaritia:resource:5>, null, null, null, null], 
	[<ore:blockInfinity>, null, null, <avaritia:resource:5>, null, null, null, null, null], 
	[<ore:blockInfinity>, null, <avaritia:resource:5>, null, null, null, null, null, null], 
	[<ore:blockInfinity>, <avaritia:resource:5>, null, null, null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:ultimate_stew>, [
	[<ore:toolPot>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <galacticraftcore:cheese>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodFishtaco>, <ore:foodSweetpotatopie>, <ore:foodStuffedpepper>, <ore:foodStuffedmushroom>, <ore:foodChocolatebar>, <ore:foodZucchinibread>, <ore:foodDimsum>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodSausagebeanmelt>, <ore:foodApricotglazedpork>, <ore:foodFriedchicken>, <ore:foodOrangejellysandwich>, <ore:foodGingerchicken>, <ore:foodFruitcrumble>, <ore:foodGreeneggsandham>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:listAllmuttoncooked>, <ore:foodGummybears>, <ore:foodHotdog>, <ore:foodHoneylemonlamb>, <ore:foodBlt>, <ore:foodCoconutshrimp>, <ore:foodTurkishdelight>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodDelightedmeal>, <ore:foodPeachcobbler>, <ore:foodChickenparmasan>, <ore:foodSpagettiandmeatballs>, <ore:foodBaklava>, <ore:foodHotwings>, <ore:foodSteakfajita>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodKoreandinner>, <ore:foodSoftpretzelandmustard>, <ore:foodCheeseburger>, <ore:foodWalnutraisinbread>, <ore:foodCreamcookie>, <ore:foodCinnamonsugardonut>, <ore:foodHeartyBreakfast>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodPloughmanslunch>, <ore:foodDeluxecheeseburger>, <ore:foodCheesecake>, <ore:foodPotatoandcheesepirogi>, <harvestcraft:pumpkincheesecakeitem>, <ore:foodSummersquashwithradish>, <ore:foodCrawfishetoufee>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:foodThankfuldinner>, <ore:foodZestyzucchini>, <ore:foodSummerradishsalad>, <ore:foodBeanburrito>, <ore:foodPineappleham>, <ore:foodGarlicchicken>, <ore:foodSupremepizza>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:cosmic_meatballs>, [
	[<ore:toolPot>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <harvestcraft:epicbltitem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <harvestcraft:thankfuldinneritem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <harvestcraft:koreandinneritem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <harvestcraft:meatfeastpizzaitem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <harvestcraft:minerstewitem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <harvestcraft:southernstylebreakfastitem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <harvestcraft:cornedbeefbreakfastitem>, <ore:toolPot>], 
	[<ore:toolPot>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <harvestcraft:gourmetvenisonburgeritem>, <ore:toolPot>], 
	[<ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>, <ore:toolPot>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <tradeasteroid:cratefluids>, [
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <openblocks:tank>.withTag({tank: {FluidName: "oil", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "cryotheum", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "redstone", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "xpjuice", Amount: 16000}}), <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <openblocks:tank>.withTag({tank: {FluidName: "meat", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "petrotheum", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "pyrotheum", Amount: 16000}}), <openblocks:tank>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <openblocks:tank>.withTag({tank: {FluidName: "aerotheum", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "creosote", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "glowstone", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "biodiesel", Amount: 16000}}), <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <openblocks:tank>.withTag({tank: {FluidName: "essence", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "milk", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "sludge", Amount: 16000}}), <openblocks:tank>.withTag({tank: {FluidName: "fuel", Amount: 16000}}), <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <tradeasteroid:cratefood>, [
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <avaritia:ultimate_stew>, <avaritia:cosmic_meatballs>, <avaritia:cosmic_meatballs>, <avaritia:ultimate_stew>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <avaritia:ultimate_stew>, <avaritia:cosmic_meatballs>, <avaritia:cosmic_meatballs>, <avaritia:ultimate_stew>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <avaritia:ultimate_stew>, <ore:plankWood>, null, null, null], 
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <tradeasteroid:cratesupply>, [
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <stevescarts:blockmetalstorage:1>, <extendedcrafting:singularity:17>, <extendedcrafting:singularity>, <extendedcrafting:singularity:18>, <stevescarts:blockmetalstorage:1>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <stevescarts:blockmetalstorage:1>, <extendedcrafting:singularity:23>, <extendedcrafting:singularity:3>, <extendedcrafting:singularity:25>, <stevescarts:blockmetalstorage:1>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <stevescarts:blockmetalstorage:1>, <extendedcrafting:singularity:1>, <extendedcrafting:singularity:4>, <extendedcrafting:singularity:16>, <stevescarts:blockmetalstorage:1>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <stevescarts:blockmetalstorage:1>, <ore:plankWood>, null, null], 
	[<ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, <ore:plankWood>, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <appliedenergistics2:creative_storage_cell>, [
	[<ore:dragonEgg>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <ore:dragonEgg>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:certustank>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <extracells:certustank>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:certustank>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <extracells:certustank>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:certustank>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <extracells:certustank>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:certustank>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<extracells:hardmedrive>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:storage.component:3>, <extracells:hardmedrive>], 
	[<ore:dragonEgg>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <extracells:hardmedrive>, <ore:dragonEgg>]
]);



mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:elite_cell>, [
	[null, null, <ore:plateMithril>, null, null], 
	[<ore:plateMithril>, <ore:plateMithril>, <thermalfoundation:material:515>, <ore:plateMithril>, <ore:plateMithril>], 
	[<ore:plateMithril>, <extraplanets:mercury_battery>, <ore:blockRedstone>, <extraplanets:mercury_battery>, <ore:plateMithril>], 
	[<ore:plateMithril>, <car:control_unit>, <thermalfoundation:material:515>, <car:control_unit>, <ore:plateMithril>], 
	[<ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:strap>, [
	[null, null, null, null, null], 
	[null, null, null, null, null], 
	[<harvestcraft:hardenedleatheritem>, <harvestcraft:hardenedleatheritem>, <ore:gearSilver>, <harvestcraft:hardenedleatheritem>, <harvestcraft:hardenedleatheritem>], 
	[null, null, null, null, null], 
	[null, null, null, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:elite_capacitor>, [
	[null, <immersiveengineering:connector:10>, null, <immersiveengineering:connector:10>, null], 
	[<ore:plateMithril>, <ore:wireCopper>, <ore:plateMithril>, <ore:wireCopper>, <ore:plateMithril>], 
	[<ore:plateMithril>, <car:control_unit>, <thermalexpansion:capacitor:4>, <car:control_unit>, <ore:plateMithril>], 
	[<ore:plateMithril>, <car:control_unit>, <ore:gearRedstone>, <car:control_unit>, <ore:plateMithril>], 
	[<ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:elite_thruster>, [
	[null, <ore:plateMithril>, <ore:plateMithril>, <ore:plateMithril>, null], 
	[<ore:plateMithril>, <ore:gearRedstone>, <car:control_unit>, <ore:gearRedstone>, <ore:plateMithril>], 
	[<ore:plateMithril>, <ore:gearSilver>, <immersiveengineering:metal_decoration0>, <ore:gearSilver>, <ore:plateMithril>], 
	[<ore:plateMithril>, <mekanismgenerators:turbineblade>, <thermalfoundation:material:513>, <mekanismgenerators:turbineblade>, <ore:plateMithril>], 
	[<ore:plateMithril>, <extraplanets:tier4_items:2>, <extraplanets:tier4_items>, <extraplanets:tier4_items:2>, <ore:plateMithril>]
]);


mods.extendedcrafting.TableCrafting.addShaped(0, <ironjetpacks:elite_jetpack>.withTag({ENABLED: 1 as byte, RARITY: 4}), [
	[null, <extraplanets:tier4_items>, null, <extraplanets:tier4_items>, null], 
	[<ore:plateMithril>, <rftools:shield_block1>, <ironjetpacks:strap>, <rftools:shield_block1>, <ore:plateMithril>], 
	[<galacticraftcore:spin_thruster>, <ore:plateMithril>, <ironjetpacks:elite_capacitor>, <ore:plateMithril>, <galacticraftcore:spin_thruster>], 
	[<extraplanets:tier4_items:2>, <ore:plateMithril>, <variedcommodities:element_air>, <ore:plateMithril>, <extraplanets:tier4_items:2>], 
	[<extraplanets:tier4_items:2>, <ironjetpacks:elite_thruster>, <ore:plateMithril>, <ironjetpacks:elite_thruster>, <extraplanets:tier4_items:2>]
]);