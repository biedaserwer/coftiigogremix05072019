
mods.thaumcraft.Crucible.registerRecipe("quickCert", "", <appliedenergistics2:material:0>, <variedcommodities:ingot_mithril>, [<aspect:aer>*4, <aspect:ignis>*2]);

mods.thaumcraft.Crucible.registerRecipe("cquickCert", "", <appliedenergistics2:material:1>, <appliedenergistics2:material:0>, [<aspect:potentia>*6]);

mods.thaumcraft.Crucible.registerRecipe("pquickCert", "", <appliedenergistics2:material:10>, <appliedenergistics2:crystal_seed:0>, [<aspect:herba>*6]);

mods.thaumcraft.Crucible.registerRecipe("coalFluix", "", <appliedenergistics2:material:7>, <minecraft:coal_block>, [<aspect:mortuus>*6]);

mods.thaumcraft.Crucible.registerRecipe("pcoalFluix", "", <appliedenergistics2:material:12>, <appliedenergistics2:material:7>, [<aspect:potentia>*6]);

mods.thaumcraft.Crucible.registerRecipe("pturdNether", "", <appliedenergistics2:material:11>, <appliedenergistics2:crystal_seed:600>, [<aspect:mortuus>*6]);


mods.thaumcraft.Crucible.registerRecipe("endMana", "", <thermalfoundation:material:136>, <thermalfoundation:material:167>, [<aspect:lux>*4, <aspect:praecantatio>*2]);