val itemFlower = <ore:itemFlower>;
itemFlower.add(<minecraft:red_flower>);
itemFlower.add(<minecraft:red_flower:1>);
itemFlower.add(<minecraft:red_flower:2>);
itemFlower.add(<minecraft:red_flower:3>);
itemFlower.add(<minecraft:red_flower:4>);
itemFlower.add(<minecraft:red_flower:5>);
itemFlower.add(<minecraft:red_flower:6>);
itemFlower.add(<minecraft:red_flower:7>);
itemFlower.add(<minecraft:red_flower:8>);
itemFlower.add(<minecraft:yellow_flower>);

val itemConcretep = <ore:itemConcretep>;
itemConcretep.add(<minecraft:concrete_powder>);
itemConcretep.add(<minecraft:concrete_powder:1>);
itemConcretep.add(<minecraft:concrete_powder:2>);
itemConcretep.add(<minecraft:concrete_powder:3>);
itemConcretep.add(<minecraft:concrete_powder:4>);
itemConcretep.add(<minecraft:concrete_powder:5>);
itemConcretep.add(<minecraft:concrete_powder:6>);
itemConcretep.add(<minecraft:concrete_powder:7>);
itemConcretep.add(<minecraft:concrete_powder:8>);
itemConcretep.add(<minecraft:concrete_powder:9>);
itemConcretep.add(<minecraft:concrete_powder:10>);
itemConcretep.add(<minecraft:concrete_powder:11>);
itemConcretep.add(<minecraft:concrete_powder:12>);
itemConcretep.add(<minecraft:concrete_powder:13>);
itemConcretep.add(<minecraft:concrete_powder:14>);
itemConcretep.add(<minecraft:concrete_powder:15>);


val oreDictMithril = <ore:ingotMithril>;
oreDictMithril.remove(<variedcommodities:ingot_mithril>);

