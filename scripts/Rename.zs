
//Example
<lootbags:itemlootbag:0>.displayName = "Armor Grab Bag";
<lootbags:itemlootbag:1>.displayName = "Tier 1 Weapon Grab Bag";
<lootbags:itemlootbag:2>.displayName = "Tier 2 Weapon Grab Bag";

<economy:block_atm>.displayName = "D.I.C.K.";
<industrialforegoing:fertilizer>.displayName = "Turd";
<economy:item_gearmecanism>.displayName = "Polymer Components";
<harvestcraft:candledeco16>.displayName = "Stone Torch";
<economy:item_gear>.displayName = "Osmium Gear";
<economy:item_oneb>.displayName = "One Biff";
<economy:item_fiveb>.displayName = "Five Biffs";
<economy:item_tenb>.displayName = "Ten Biffs";
<economy:item_twentyb>.displayName = "Twenty Biffs";
<economy:item_fiftybe>.displayName = "Fifty Biffs";
<economy:item_hundreedb>.displayName = "One Hundred Biffs";
<economy:item_twohundreedb>.displayName = "Two Hundred Biffs";
<economy:item_fivehundreedb>.displayName = "Five Hundred Biffs";
<economy:item_packetone>.displayName = "One Biff Bundle";
<economy:item_packetfive>.displayName = "Five Biff Bundle";
<economy:item_packetten>.displayName = "Ten Biff Bundle";
<economy:item_packettwenty>.displayName = "Twenty Biff Bundle";
<economy:item_packetfifty>.displayName = "Fifty Biff Bundle";
<economy:item_packethundreed>.displayName = "One Hundred Biff Bundle";
<economy:item_packettwohundreed>.displayName = "Two Hundred Biff Bundle";
<economy:item_packetfivehundreed>.displayName = "Five Hundred Biff Bundle";
<spikemod:hot_spike>.displayName = "Apocalypse Spike";
<spikemod:freezing_spike>.displayName = "Thermic Spike";
<spikemod:looting_spike>.displayName = "Blessed Spike";
<variedcommodities:orb:5>.displayName = "Crafting Altar Core";
<variedcommodities:orb:4>.displayName = "Mana Battery Core";
<extendedcrafting:table_advanced>.displayName = "Jetpack Workstation";
<variedcommodities:element_air>.displayName = "Gravity Well";
<from_the_depths:block_altar_of_summoning>.displayName = "Time Anchor";
<draconicevolution:infused_obsidian>.displayName = "Wither Proof Sigsidian";
<variedcommodities:mana>.displayName = "Quicksilver Dust";
<variedcommodities:ingot_mithril>.displayName = "Quicksilver Ingot";
<ezstorage:blank_box>.displayName = "Storage Node";


mods.thermalexpansion.InductionSmelter.addRecipe(<enderutilities:enderpart>, <thermalfoundation:material:129>, <minecraft:ender_pearl>, 3500);
mods.thermalexpansion.InductionSmelter.addRecipe(<enderutilities:enderpart:1>, <thermalfoundation:material:130>, <minecraft:ender_pearl>, 3500);
mods.thermalexpansion.InductionSmelter.addRecipe(<enderutilities:enderpart:2>, <minecraft:gold_ingot>, <minecraft:ender_pearl>, 3500);
mods.thermalexpansion.InductionSmelter.addRecipe(<extraplanets:tier5_items:5>, <actuallyadditions:item_crystal_empowered:1>, <thermalfoundation:material:132>, 5500);
mods.thermalexpansion.InductionSmelter.addRecipe(<extraplanets:tier5_items:5>, <actuallyadditions:item_crystal_empowered:1>, <galacticraftcore:basic_item:5>, 5500);
mods.thermalexpansion.InductionSmelter.addRecipe(<extraplanets:tier5_items:5>, <actuallyadditions:item_crystal_empowered:1>, <immersiveengineering:metal:1>, 5500);
mods.thermalexpansion.InductionSmelter.removeRecipe(<galacticraftplanets:venus:8>, <thermalfoundation:material:866>);
mods.thermalexpansion.InductionSmelter.removeRecipe(<galacticraftplanets:venus:8>, <thermalfoundation:material:865>);
mods.thermalexpansion.InductionSmelter.removeRecipe(<galacticraftplanets:venus:8>, <minecraft:sand>);


mods.mekanism.separator.removeRecipe(<liquid:water>, <gas:hydrogen>, <gas:oxygen>);
mods.mekanism.separator.addRecipe(<liquid:creosote>, 3000, <gas:hydrogen>, <gas:oxygen>);
mods.mekanism.separator.addRecipe(<liquid:sludge>, 3000, <gas:hydrogen>, <gas:oxygen>);
mods.mekanism.separator.addRecipe(<liquid:water>, 9000, <gas:hydrogen>, <gas:oxygen>);

mods.armorplus.Workbench.remove(<armorplus:compressed_obsidian>);


//lore

<storagedrawers:compdrawers:0>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:customdrawers:0>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:customdrawers:1>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:customdrawers:2>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:basicdrawers:0>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:basicdrawers:1>.addTooltip(format.yellow("Found in Lootbags"));
<storagedrawers:basicdrawers:2>.addTooltip(format.yellow("Found in Lootbags"));