mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalfoundation:material:514>, 50000, 400, <thermalfoundation:material:512>, [<industrialforegoing:plastic>, <minecraft:redstone>, <immersiveengineering:material:20>, <ore:ingotNickel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalfoundation:material:515>, 50000, 400, <thermalfoundation:material:512>, [<industrialforegoing:plastic>, <minecraft:redstone>, <immersiveengineering:material:20>, <ore:ingotCopper>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalfoundation:material:513>, 50000, 400, <thermalfoundation:material:512>, [<industrialforegoing:plastic>, <minecraft:redstone>, <immersiveengineering:material:20>, <ore:ingotIron>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:frame:128>, 50000, 400, <thermalexpansion:frame:0>, [<ore:blockGlass>, <thermalfoundation:material:515>, <immersiveengineering:material:20>, <ore:gearSteel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:frame:129>, 50000, 400, <thermalexpansion:frame:128>, [<ore:gearNickel>, <thermalfoundation:material:515>, <immersiveengineering:material:20>, <ore:gearSteel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:frame:130>, 70000, 400, <thermalexpansion:frame:129>, [<ore:gearElectrum>, <thermalfoundation:material:515>, <immersiveengineering:material:20>, <ore:gearSteel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:frame:131>, 90000, 600, <thermalexpansion:frame:130>, [<ore:gearSignalum>, <thermalfoundation:material:515>, <immersiveengineering:material:20>, <ore:gearSteel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:frame:132>, 100000, 900, <thermalexpansion:frame:131>, [<ore:gearEnderium>, <thermalfoundation:material:515>, <immersiveengineering:material:20>, <ore:gearSteel>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<car:control_unit>, 50000, 350, <industrialforegoing:plastic>, [<ore:wireCopper>, <thermalfoundation:material:515>, <minecraft:redstone>, <minecraft:repeater>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<car:engine_3_cylinder>, 100000, 800, <thermalfoundation:storage_alloy>, [<car:engine_piston>, <car:engine_piston>, <car:engine_piston>, <car:blastfurnace>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<car:engine_6_cylinder>, 200000, 800, <thermalfoundation:storage_alloy>, [<car:engine_3_cylinder>, <car:engine_3_cylinder>, <immersiveengineering:metal_device1:6>, <immersiveengineering:metal_device1:6>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<enderutilities:linkcrystal>, 200000, 800, <minecraft:diamond_block>, [<minecraft:ender_pearl>, <minecraft:ender_pearl>, <minecraft:ender_pearl>, <minecraft:map>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<enderutilities:linkcrystal:1>, 200000, 800, <minecraft:diamond_block>, [<minecraft:ender_pearl>, <minecraft:ender_pearl>, <minecraft:ender_pearl>, <minecraft:clock>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<extrautils2:klein>, 200000, 800, <minecraft:glass_bottle>, [<ore:blockIce>, <minecraft:ender_pearl>, <avaritia:resource>, <thermalfoundation:material:1025>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<minecraft:skull:1>, 200000, 800, <minecraft:skull:0>, [<ore:blockIce>, <minecraft:ender_eye>, <minecraft:coal_block>, <actuallyadditions:block_misc:2>]);