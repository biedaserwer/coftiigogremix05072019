//Machine Recipe Changes



//Crafting Recipe Changes
recipes.remove(<thermalfoundation:material:514>);
recipes.remove(<thermalfoundation:material:513>);
recipes.remove(<thermalfoundation:material:515>);
recipes.remove(<thermalexpansion:machine>);
recipes.remove(<thermalexpansion:machine:1>);
recipes.remove(<thermalexpansion:machine:2>);
recipes.remove(<thermalexpansion:machine:3>);
recipes.remove(<thermalexpansion:machine:4>);
recipes.remove(<thermalexpansion:machine:5>);
recipes.remove(<thermalexpansion:machine:6>);
recipes.remove(<thermalexpansion:machine:7>);
recipes.remove(<thermalexpansion:machine:8>);
recipes.remove(<thermalexpansion:machine:9>);
recipes.remove(<thermalexpansion:machine:10>);
recipes.remove(<thermalexpansion:machine:14>);
recipes.remove(<thermalexpansion:machine:15>);
recipes.remove(<thermalfoundation:upgrade:34>);
recipes.remove(<thermalfoundation:upgrade:33>);
recipes.remove(<thermalfoundation:upgrade:3>);
recipes.remove(<thermalfoundation:upgrade:2>);
recipes.remove(<thermalfoundation:upgrade:1>);
recipes.remove(<thermalfoundation:upgrade>);
recipes.remove(<thermalfoundation:upgrade:35>);
recipes.remove(<thermalexpansion:capacitor:3>);
recipes.remove(<thermalexpansion:capacitor:4>);
recipes.remove(<thermalexpansion:capacitor:2>);
recipes.remove(<thermalexpansion:capacitor:1>);
recipes.remove(<thermalexpansion:capacitor>);
recipes.remove(<thermaldynamics:duct_16:7>);
recipes.remove(<thermaldynamics:duct_16:6>);
recipes.remove(<thermaldynamics:duct_16:5>);
recipes.remove(<thermaldynamics:duct_16:4>);
recipes.remove(<thermaldynamics:duct_16:3>);
recipes.remove(<thermaldynamics:duct_16:2>);
recipes.remove(<thermaldynamics:duct_16:1>);
recipes.remove(<thermaldynamics:duct_16>);
recipes.remove(<thermaldynamics:duct_32:7>);
recipes.remove(<thermaldynamics:duct_32:6>);
recipes.remove(<thermaldynamics:duct_32:3>);
recipes.remove(<thermaldynamics:duct_32:2>);
recipes.remove(<thermaldynamics:duct_32:5>);
recipes.remove(<thermaldynamics:duct_32:4>);
recipes.remove(<thermaldynamics:duct_32:1>);
recipes.remove(<thermaldynamics:duct_32>);
recipes.remove(<thermaldynamics:duct_0>);
recipes.remove(<thermaldynamics:duct_0:1>);
recipes.remove(<thermaldynamics:duct_0:2>);
recipes.remove(<thermaldynamics:duct_0:3>);
recipes.remove(<thermaldynamics:duct_0:4>);
recipes.remove(<thermaldynamics:duct_0:7>);
recipes.remove(<thermaldynamics:duct_0:8>);
recipes.remove(<thermaldynamics:duct_0:6>);
recipes.remove(<extrautils2:machine>);

//Redstone Furnace
recipes.addShaped(<thermalexpansion:machine>, [[null, <minecraft:redstone>, null],[<minecraft:brick_block>, <thermalexpansion:frame:128>, <minecraft:brick_block>], [<thermalfoundation:material:256>, <thermalfoundation:material:513>, <thermalfoundation:material:256>]]);

recipes.addShaped(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:redstone>, null],[<minecraft:brick_block>, <thermalexpansion:frame:129>, <minecraft:brick_block>], [<thermalfoundation:material:256>, <thermalfoundation:material:513>, <thermalfoundation:material:256>]]);


recipes.addShaped(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:redstone>, null],[<minecraft:brick_block>, <thermalexpansion:frame:130>, <minecraft:brick_block>], [<thermalfoundation:material:256>, <thermalfoundation:material:513>, <thermalfoundation:material:256>]]);


recipes.addShaped(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:redstone>, null],[<minecraft:brick_block>, <thermalexpansion:frame:131>, <minecraft:brick_block>], [<thermalfoundation:material:256>, <thermalfoundation:material:513>, <thermalfoundation:material:256>]]);


recipes.addShaped(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:redstone>, null],[<minecraft:brick_block>, <thermalexpansion:frame:132>, <minecraft:brick_block>], [<thermalfoundation:material:256>, <thermalfoundation:material:513>, <thermalfoundation:material:256>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);


//Fluid Transposer
recipes.addShaped(<thermalexpansion:machine:8>, [[null, <minecraft:bucket>, null],[<ore:blockGlass>, <thermalexpansion:frame:128>, <ore:blockGlass>], [<thermalfoundation:material:290>, <thermalfoundation:material:513>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<ore:blockGlass>, <thermalexpansion:frame:129>, <ore:blockGlass>], [<thermalfoundation:material:290>, <thermalfoundation:material:513>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<ore:blockGlass>, <thermalexpansion:frame:130>, <ore:blockGlass>], [<thermalfoundation:material:290>, <thermalfoundation:material:513>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<ore:blockGlass>, <thermalexpansion:frame:131>, <ore:blockGlass>], [<thermalfoundation:material:290>, <thermalfoundation:material:513>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<ore:blockGlass>, <thermalexpansion:frame:132>, <ore:blockGlass>], [<thermalfoundation:material:290>, <thermalfoundation:material:513>, <thermalfoundation:material:290>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:8>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:8>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);


//Pulverizer
recipes.addShaped(<thermalexpansion:machine:1>, [[null, <minecraft:piston>, null],[<minecraft:flint>, <thermalexpansion:frame:128>, <minecraft:flint>], [<thermalfoundation:material:261>, <thermalfoundation:material:515>, <thermalfoundation:material:261>]]);

recipes.addShaped(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:piston>, null],[<minecraft:flint>, <thermalexpansion:frame:129>, <minecraft:flint>], [<thermalfoundation:material:261>, <thermalfoundation:material:515>, <thermalfoundation:material:261>]]);

recipes.addShaped(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:piston>, null],[<minecraft:flint>, <thermalexpansion:frame:130>, <minecraft:flint>], [<thermalfoundation:material:261>, <thermalfoundation:material:515>, <thermalfoundation:material:261>]]);

recipes.addShaped(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:piston>, null],[<minecraft:flint>, <thermalexpansion:frame:131>, <minecraft:flint>], [<thermalfoundation:material:261>, <thermalfoundation:material:515>, <thermalfoundation:material:261>]]);

recipes.addShaped(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:piston>, null],[<minecraft:flint>, <thermalexpansion:frame:132>, <minecraft:flint>], [<thermalfoundation:material:261>, <thermalfoundation:material:515>, <thermalfoundation:material:261>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:1>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:1>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Sawmill
recipes.addShaped(<thermalexpansion:machine:2>, [[null, <thermalfoundation:material:290>, null],[<immersiveengineering:treated_wood>, <thermalexpansion:frame:128>, <immersiveengineering:treated_wood>], [<immersiveengineering:treated_wood>, <thermalfoundation:material:515>, <immersiveengineering:treated_wood>]]);

recipes.addShaped(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:290>, null],[<immersiveengineering:treated_wood>, <thermalexpansion:frame:129>, <immersiveengineering:treated_wood>], [<immersiveengineering:treated_wood>, <thermalfoundation:material:515>, <immersiveengineering:treated_wood>]]);

recipes.addShaped(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:290>, null],[<immersiveengineering:treated_wood>, <thermalexpansion:frame:130>, <immersiveengineering:treated_wood>], [<immersiveengineering:treated_wood>, <thermalfoundation:material:515>, <immersiveengineering:treated_wood>]]);

recipes.addShaped(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:290>, null],[<immersiveengineering:treated_wood>, <thermalexpansion:frame:131>, <immersiveengineering:treated_wood>], [<immersiveengineering:treated_wood>, <thermalfoundation:material:515>, <immersiveengineering:treated_wood>]]);

recipes.addShaped(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:290>, null],[<immersiveengineering:treated_wood>, <thermalexpansion:frame:132>, <immersiveengineering:treated_wood>], [<immersiveengineering:treated_wood>, <thermalfoundation:material:515>, <immersiveengineering:treated_wood>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:2>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:2>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Induction Smelter
recipes.addShaped(<thermalexpansion:machine:3>, [[null, <minecraft:bucket>, null],[<thermalfoundation:material:162>, <thermalexpansion:frame:128>, <thermalfoundation:material:162>], [<thermalfoundation:material:290>, <thermalfoundation:material:515>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<thermalfoundation:material:162>, <thermalexpansion:frame:129>, <thermalfoundation:material:162>], [<thermalfoundation:material:290>, <thermalfoundation:material:515>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<thermalfoundation:material:162>, <thermalexpansion:frame:130>, <thermalfoundation:material:162>], [<thermalfoundation:material:290>, <thermalfoundation:material:515>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<thermalfoundation:material:162>, <thermalexpansion:frame:131>, <thermalfoundation:material:162>], [<thermalfoundation:material:290>, <thermalfoundation:material:515>, <thermalfoundation:material:290>]]);

recipes.addShaped(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:bucket>, null],[<thermalfoundation:material:162>, <thermalexpansion:frame:132>, <thermalfoundation:material:162>], [<thermalfoundation:material:290>, <thermalfoundation:material:515>, <thermalfoundation:material:290>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:3>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:3>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Photogenic Isolator
recipes.addShaped(<thermalexpansion:machine:4>, [[null, <thermalfoundation:material:294>, null],[<extrautils2:compresseddirt:1>, <thermalexpansion:frame:128>, <extrautils2:compresseddirt:1>], [<thermalfoundation:material:294>, <thermalfoundation:material:514>, <thermalfoundation:material:294>]]);

recipes.addShaped(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:294>, null],[<extrautils2:compresseddirt:1>, <thermalexpansion:frame:129>, <extrautils2:compresseddirt:1>], [<thermalfoundation:material:294>, <thermalfoundation:material:514>, <thermalfoundation:material:294>]]);

recipes.addShaped(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:294>, null],[<extrautils2:compresseddirt:1>, <thermalexpansion:frame:130>, <extrautils2:compresseddirt:1>], [<thermalfoundation:material:294>, <thermalfoundation:material:514>, <thermalfoundation:material:294>]]);

recipes.addShaped(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:294>, null],[<extrautils2:compresseddirt:1>, <thermalexpansion:frame:131>, <extrautils2:compresseddirt:1>], [<thermalfoundation:material:294>, <thermalfoundation:material:514>, <thermalfoundation:material:294>]]);

recipes.addShaped(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:294>, null],[<extrautils2:compresseddirt:1>, <thermalexpansion:frame:132>, <extrautils2:compresseddirt:1>], [<thermalfoundation:material:294>, <thermalfoundation:material:514>, <thermalfoundation:material:294>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:4>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:4>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);


//Compactor
recipes.addShaped(<thermalexpansion:machine:5>, [[null, <thermalfoundation:material:259>, null],[<minecraft:piston>, <thermalexpansion:frame:128>, <minecraft:piston>], [<thermalfoundation:material:259>, <thermalfoundation:material:514>, <thermalfoundation:material:259>]]);

recipes.addShaped(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:259>, null],[<minecraft:piston>, <thermalexpansion:frame:129>, <minecraft:piston>], [<thermalfoundation:material:259>, <thermalfoundation:material:514>, <thermalfoundation:material:259>]]);

recipes.addShaped(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:259>, null],[<minecraft:piston>, <thermalexpansion:frame:130>, <minecraft:piston>], [<thermalfoundation:material:259>, <thermalfoundation:material:514>, <thermalfoundation:material:259>]]);

recipes.addShaped(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:259>, null],[<minecraft:piston>, <thermalexpansion:frame:131>, <minecraft:piston>], [<thermalfoundation:material:259>, <thermalfoundation:material:514>, <thermalfoundation:material:259>]]);

recipes.addShaped(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:259>, null],[<minecraft:piston>, <thermalexpansion:frame:132>, <minecraft:piston>], [<thermalfoundation:material:259>, <thermalfoundation:material:514>, <thermalfoundation:material:259>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:5>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:5>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Magma Crucible
recipes.addShaped(<thermalexpansion:machine:6>, [[null, <thermalfoundation:material:515>, null],[<thermalfoundation:material:1024>, <thermalexpansion:frame:128>, <thermalfoundation:material:1024>], [<thermalfoundation:material:260>, <thermalfoundation:material:513>, <thermalfoundation:material:260>]]);

recipes.addShaped(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:515>, null],[<thermalfoundation:material:1024>, <thermalexpansion:frame:129>, <thermalfoundation:material:1024>], [<thermalfoundation:material:260>, <thermalfoundation:material:513>, <thermalfoundation:material:260>]]);

recipes.addShaped(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:515>, null],[<thermalfoundation:material:1024>, <thermalexpansion:frame:130>, <thermalfoundation:material:1024>], [<thermalfoundation:material:260>, <thermalfoundation:material:513>, <thermalfoundation:material:260>]]);

recipes.addShaped(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:515>, null],[<thermalfoundation:material:1024>, <thermalexpansion:frame:131>, <thermalfoundation:material:1024>], [<thermalfoundation:material:260>, <thermalfoundation:material:513>, <thermalfoundation:material:260>]]);

recipes.addShaped(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:515>, null],[<thermalfoundation:material:1024>, <thermalexpansion:frame:132>, <thermalfoundation:material:1024>], [<thermalfoundation:material:260>, <thermalfoundation:material:513>, <thermalfoundation:material:260>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:6>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:6>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Fractioning Still
recipes.addShaped(<thermalexpansion:machine:7>, [[null, <thermalfoundation:material:257>, null],[<minecraft:glass_bottle>, <thermalexpansion:frame:128>, <minecraft:glass_bottle>], [<thermalfoundation:material:257>, <thermalfoundation:material:515>, <thermalfoundation:material:257>]]);

recipes.addShaped(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [[null, <thermalfoundation:material:257>, null],[<minecraft:glass_bottle>, <thermalexpansion:frame:129>, <minecraft:glass_bottle>], [<thermalfoundation:material:257>, <thermalfoundation:material:515>, <thermalfoundation:material:257>]]);

recipes.addShaped(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [[null, <thermalfoundation:material:257>, null],[<minecraft:glass_bottle>, <thermalexpansion:frame:130>, <minecraft:glass_bottle>], [<thermalfoundation:material:257>, <thermalfoundation:material:515>, <thermalfoundation:material:257>]]);

recipes.addShaped(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [[null, <thermalfoundation:material:257>, null],[<minecraft:glass_bottle>, <thermalexpansion:frame:131>, <minecraft:glass_bottle>], [<thermalfoundation:material:257>, <thermalfoundation:material:515>, <thermalfoundation:material:257>]]);

recipes.addShaped(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [[null, <thermalfoundation:material:257>, null],[<minecraft:glass_bottle>, <thermalexpansion:frame:132>, <minecraft:glass_bottle>], [<thermalfoundation:material:257>, <thermalfoundation:material:515>, <thermalfoundation:material:257>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:7>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:7>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 2, 3, 0, 3, 3] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Energetic Infuser
recipes.addShaped(<thermalexpansion:machine:9>, [[null, <thermalfoundation:material:513>, null],[<thermalfoundation:material:512>, <thermalexpansion:frame:128>, <thermalfoundation:material:512>], [<thermalfoundation:material:288>, <thermalfoundation:material:515>, <thermalfoundation:material:288>]]);

recipes.addShaped(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:513>, null],[<thermalfoundation:material:512>, <thermalexpansion:frame:129>, <thermalfoundation:material:512>], [<thermalfoundation:material:288>, <thermalfoundation:material:515>, <thermalfoundation:material:288>]]);

recipes.addShaped(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:513>, null],[<thermalfoundation:material:512>, <thermalexpansion:frame:130>, <thermalfoundation:material:512>], [<thermalfoundation:material:288>, <thermalfoundation:material:515>, <thermalfoundation:material:288>]]);

recipes.addShaped(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:513>, null],[<thermalfoundation:material:512>, <thermalexpansion:frame:131>, <thermalfoundation:material:512>], [<thermalfoundation:material:288>, <thermalfoundation:material:515>, <thermalfoundation:material:288>]]);

recipes.addShaped(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:513>, null],[<thermalfoundation:material:512>, <thermalexpansion:frame:132>, <thermalfoundation:material:512>], [<thermalfoundation:material:288>, <thermalfoundation:material:515>, <thermalfoundation:material:288>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:9>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:9>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Centrifugal Seperator
recipes.addShaped(<thermalexpansion:machine:10>, [[null, <minecraft:compass>, null],[<thermalfoundation:material:1027>, <thermalexpansion:frame:128>, <thermalfoundation:material:1027>], [<thermalfoundation:material:292>, <thermalfoundation:material:514>, <thermalfoundation:material:292>]]);

recipes.addShaped(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:compass>, null],[<thermalfoundation:material:1027>, <thermalexpansion:frame:129>, <thermalfoundation:material:1027>], [<thermalfoundation:material:292>, <thermalfoundation:material:514>, <thermalfoundation:material:292>]]);

recipes.addShaped(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:compass>, null],[<thermalfoundation:material:1027>, <thermalexpansion:frame:130>, <thermalfoundation:material:1027>], [<thermalfoundation:material:292>, <thermalfoundation:material:514>, <thermalfoundation:material:292>]]);

recipes.addShaped(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:compass>, null],[<thermalfoundation:material:1027>, <thermalexpansion:frame:131>, <thermalfoundation:material:1027>], [<thermalfoundation:material:292>, <thermalfoundation:material:514>, <thermalfoundation:material:292>]]);

recipes.addShaped(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <minecraft:compass>, null],[<thermalfoundation:material:1027>, <thermalexpansion:frame:132>, <thermalfoundation:material:1027>], [<thermalfoundation:material:292>, <thermalfoundation:material:514>, <thermalfoundation:material:292>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:10>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:10>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [3, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Glacial Precipitator
recipes.addShaped(<thermalexpansion:machine:14>, [[<minecraft:snow>, <thermalfoundation:material:258>, <minecraft:snow>],[<thermalfoundation:material:1025>, <thermalexpansion:frame:128>, <thermalfoundation:material:1025>], [<minecraft:snow>, <thermalfoundation:material:514>, <minecraft:snow>]]);

recipes.addShaped(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[<minecraft:snow>, <thermalfoundation:material:258>, <minecraft:snow>],[<thermalfoundation:material:1025>, <thermalexpansion:frame:129>, <thermalfoundation:material:1025>], [<minecraft:snow>, <thermalfoundation:material:514>, <minecraft:snow>]]);

recipes.addShaped(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[<minecraft:snow>, <thermalfoundation:material:258>, <minecraft:snow>],[<thermalfoundation:material:1025>, <thermalexpansion:frame:130>, <thermalfoundation:material:1025>], [<minecraft:snow>, <thermalfoundation:material:514>, <minecraft:snow>]]);

recipes.addShaped(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[<minecraft:snow>, <thermalfoundation:material:258>, <minecraft:snow>],[<thermalfoundation:material:1025>, <thermalexpansion:frame:131>, <thermalfoundation:material:1025>], [<minecraft:snow>, <thermalfoundation:material:514>, <minecraft:snow>]]);

recipes.addShaped(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[<minecraft:snow>, <thermalfoundation:material:258>, <minecraft:snow>],[<thermalfoundation:material:1025>, <thermalexpansion:frame:132>, <thermalfoundation:material:1025>], [<minecraft:snow>, <thermalfoundation:material:514>, <minecraft:snow>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:14>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:14>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Igneous Extruder
recipes.addShaped(<thermalexpansion:machine:15>, [[null, <thermalfoundation:material:292>, null],[<thermalfoundation:material:1026>, <thermalexpansion:frame:128>, <thermalfoundation:material:1026>], [<minecraft:glass_bottle>, <thermalfoundation:material:515>, <minecraft:glass_bottle>]]);

recipes.addShaped(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:292>, null],[<thermalfoundation:material:1026>, <thermalexpansion:frame:129>, <thermalfoundation:material:1026>], [<minecraft:glass_bottle>, <thermalfoundation:material:515>, <minecraft:glass_bottle>]]);

recipes.addShaped(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:292>, null],[<thermalfoundation:material:1026>, <thermalexpansion:frame:130>, <thermalfoundation:material:1026>], [<minecraft:glass_bottle>, <thermalfoundation:material:515>, <minecraft:glass_bottle>]]);

recipes.addShaped(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:292>, null],[<thermalfoundation:material:1026>, <thermalexpansion:frame:131>, <thermalfoundation:material:1026>], [<minecraft:glass_bottle>, <thermalfoundation:material:515>, <minecraft:glass_bottle>]]);

recipes.addShaped(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [[null, <thermalfoundation:material:292>, null],[<thermalfoundation:material:1026>, <thermalexpansion:frame:132>, <thermalfoundation:material:1026>], [<minecraft:glass_bottle>, <thermalfoundation:material:515>, <minecraft:glass_bottle>]]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:15>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), 300000, 650, <thermalexpansion:machine:15>.withTag({RSControl: 0 as byte, Facing: 3 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: [], SideCache: [1, 1, 2, 0, 2, 2] as byte[] as byte[]}), [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Combination Craftifoundation Upgrades
mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:5>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:5>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:5>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:5>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:5>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:5>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:5>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:5>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:1>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:1>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:1>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:1>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:1>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:1>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:1>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:1>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:2>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:2>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:2>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:2>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:2>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:2>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:2>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:2>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:3>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:3>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:3>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:3>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:3>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:3>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:3>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:3>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:4>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:4>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:4>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:4>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:4>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:4>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:4>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:4>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 1 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:0>, [<ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>, <ore:ingotInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 2 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:0>, [<ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>, <ore:ingotElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 3 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:0>, [<ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>, <ore:ingotSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:dynamo:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Energy: 0, Level: 4 as byte, Augments: []}), 300000, 650, <thermalexpansion:dynamo:0>, [<ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>, <ore:ingotEnderium>]);

//Container Combination Crafting
mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:tank:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 1 as byte}), 300000, 650, <thermalexpansion:tank:0>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:tank:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 2 as byte}), 300000, 650, <thermalexpansion:tank:0>, [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:tank:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 3 as byte}), 300000, 650, <thermalexpansion:tank:0>, [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:tank:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 4 as byte}), 300000, 650, <thermalexpansion:tank:0>, [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:cache:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 1 as byte}), 300000, 650, <thermalexpansion:cache:0>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:cache:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 2 as byte}), 300000, 650, <thermalexpansion:cache:0>, [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:cache:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 3 as byte}), 300000, 650, <thermalexpansion:cache:0>, [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:cache:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 4 as byte}), 300000, 650, <thermalexpansion:cache:0>, [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:strongbox:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 1 as byte}), 300000, 650, <thermalexpansion:strongbox:0>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:strongbox:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 2 as byte}), 300000, 650, <thermalexpansion:strongbox:0>, [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:strongbox:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 3 as byte}), 300000, 650, <thermalexpansion:strongbox:0>, [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:strongbox:0>.withTag({RSControl: 0 as byte, Creative: 0 as byte, Level: 4 as byte}), 300000, 650, <thermalexpansion:strongbox:0>, [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Satchel Combination Crafting
mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:satchel:1>.withTag({Accessible: 1 as byte}), 300000, 650, <thermalexpansion:satchel:0>, [<ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>, <ore:gearInvar>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:satchel:2>.withTag({Accessible: 1 as byte}), 300000, 650, <thermalexpansion:satchel:0>, [<ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>, <ore:gearElectrum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:satchel:3>.withTag({Accessible: 1 as byte}), 300000, 650, <thermalexpansion:satchel:0>, [<ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>, <ore:gearSignalum>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:satchel:4>.withTag({Accessible: 1 as byte}), 300000, 650, <thermalexpansion:satchel:0>, [<ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>, <ore:gearEnderium>]);

//Capacitor Combination Crafting
mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:capacitor>.withTag({Energy: 0}), 300000, 400, <industrialforegoing:plastic>, [<ore:gearNickel>, <ore:gearInvar>, <minecraft:redstone>, <car:control_unit>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:capacitor:1>.withTag({Energy: 0}), 300000, 400, <thermalexpansion:capacitor>.withTag({Energy: 0}), [<ore:gearInvar>, <ore:gearDiamond>, <minecraft:redstone>, <thermaldynamics:duct_0>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:capacitor:2>.withTag({Energy: 0}), 500000, 400, <thermalexpansion:capacitor:1>.withTag({Energy: 0}), [<ore:gearElectrum>, <ore:gearDiamond>, <minecraft:redstone>, <thermaldynamics:duct_0>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:capacitor:3>.withTag({Energy: 0}), 500000, 400, <thermalexpansion:capacitor:2>.withTag({Energy: 0}), [<ore:gearSignalum>, <ore:gearDiamond>, <minecraft:redstone>, <thermaldynamics:duct_0>]);

mods.extendedcrafting.CombinationCrafting.addRecipe(<thermalexpansion:capacitor:4>.withTag({Energy: 0}), 500000, 400, <thermalexpansion:capacitor:3>.withTag({Energy: 0}), [<ore:gearEnderium>, <ore:gearDiamond>, <minecraft:redstone>, <thermaldynamics:duct_0>]);

//Duct Crafting
recipes.addShaped(<thermaldynamics:duct_16:1> * 8, [[<thermaldynamics:duct_16>, <thermaldynamics:duct_16>, <thermaldynamics:duct_16>],[<thermaldynamics:duct_16>, <immersiveengineering:material:5>, <thermaldynamics:duct_16>], [<thermaldynamics:duct_16>, <thermaldynamics:duct_16>, <thermaldynamics:duct_16>]]);
recipes.addShaped(<thermaldynamics:duct_16:3> * 8, [[<thermaldynamics:duct_16:2>, <thermaldynamics:duct_16:2>, <thermaldynamics:duct_16:2>],[<thermaldynamics:duct_16:2>, <immersiveengineering:material:5>, <thermaldynamics:duct_16:2>], [<thermaldynamics:duct_16:2>, <thermaldynamics:duct_16:2>, <thermaldynamics:duct_16:2>]]);
recipes.addShaped(<thermaldynamics:duct_16:5> * 8, [[<thermaldynamics:duct_16:4>, <thermaldynamics:duct_16:4>, <thermaldynamics:duct_16:4>],[<thermaldynamics:duct_16:4>, <immersiveengineering:material:5>, <thermaldynamics:duct_16:4>], [<thermaldynamics:duct_16:4>, <thermaldynamics:duct_16:4>, <thermaldynamics:duct_16:4>]]);
recipes.addShaped(<thermaldynamics:duct_16:7> * 8, [[<thermaldynamics:duct_16:6>, <thermaldynamics:duct_16:6>, <thermaldynamics:duct_16:6>],[<thermaldynamics:duct_16:6>, <immersiveengineering:material:5>, <thermaldynamics:duct_16:6>], [<thermaldynamics:duct_16:6>, <thermaldynamics:duct_16:6>, <thermaldynamics:duct_16:6>]]);
recipes.addShaped(<thermaldynamics:duct_16:6> * 3, [[<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:167>, <minecraft:glass>, <thermalfoundation:material:167>], [<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_16:4> * 3, [[<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:165>, <minecraft:glass>, <thermalfoundation:material:165>], [<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_16:2> * 3, [[<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:162>, <minecraft:glass>, <thermalfoundation:material:162>], [<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_16> * 3, [[<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:131>, <minecraft:glass>, <thermalfoundation:material:131>], [<industrialforegoing:dryrubber>, <minecraft:glass>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_32:3> * 8, [[<thermaldynamics:duct_32:2>, <thermaldynamics:duct_32:2>, <thermaldynamics:duct_32:2>],[<thermaldynamics:duct_32:2>, <immersiveengineering:material:5>, <thermaldynamics:duct_32:2>], [<thermaldynamics:duct_32:2>, <thermaldynamics:duct_32:2>, <thermaldynamics:duct_32:2>]]);
recipes.addShaped(<thermaldynamics:duct_32:2> * 3, [[<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:161>, <minecraft:glass>, <thermalfoundation:material:161>], [<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_32:4> * 3, [[<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:165>, <minecraft:glass>, <thermalfoundation:material:165>], [<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_32:5> * 8, [[<thermaldynamics:duct_32:4>, <thermaldynamics:duct_32:4>, <thermaldynamics:duct_32:4>],[<thermaldynamics:duct_32:4>, <immersiveengineering:material:5>, <thermaldynamics:duct_32:4>], [<thermaldynamics:duct_32:4>, <thermaldynamics:duct_32:4>, <thermaldynamics:duct_32:4>]]);
recipes.addShaped(<thermaldynamics:duct_32:1> * 8, [[<thermaldynamics:duct_32>, <thermaldynamics:duct_32>, <thermaldynamics:duct_32>],[<thermaldynamics:duct_32>, <immersiveengineering:material:5>, <thermaldynamics:duct_32>], [<thermaldynamics:duct_32>, <thermaldynamics:duct_32>, <thermaldynamics:duct_32>]]);
recipes.addShaped(<thermaldynamics:duct_32> * 3, [[<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>],[<thermalfoundation:material:162>, <minecraft:glass>, <thermalfoundation:material:162>], [<industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_0:7>, [[null, <thermalfoundation:material:229>, null],[<ore:nuggetSignalum>, <thermaldynamics:duct_0:2>, <ore:nuggetSignalum>], [null, <thermalfoundation:material:229>, null]]);
recipes.addShaped(<thermaldynamics:duct_0:8>, [[null, <thermalfoundation:material:231>, null],[<ore:nuggetEnderium>, <thermaldynamics:duct_0:3>, <ore:nuggetEnderium>], [null, <thermalfoundation:material:231>, null]]);
recipes.addShaped(<thermaldynamics:duct_0:8> * 3, [[<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>],[<ore:ingotEnderium>, <ore:blockGlassHardened>, <ore:ingotEnderium>], [<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_0:7> * 3, [[<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>],[<ore:ingotSignalum>, <ore:blockGlassHardened>, <ore:ingotSignalum>], [<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_0:6> * 3, [[<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>],[<ore:ingotElectrum>, <ore:blockGlassHardened>, <ore:ingotElectrum>], [<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_0:1> * 3, [[<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>],[<ore:ingotInvar>, <ore:blockGlass>, <ore:ingotInvar>], [<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>]]);
recipes.addShaped(<thermaldynamics:duct_0> * 3, [[<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>],[<ore:ingotLead>, <ore:blockGlass>, <ore:ingotLead>], [<industrialforegoing:dryrubber>, <minecraft:redstone>, <industrialforegoing:dryrubber>]]);

recipes.addShaped(<extrautils2:machine>, [[<ore:ingotSteel>, <ore:plateAluminum>, <ore:ingotSteel>],[<ore:plateAluminum>, <thermalfoundation:material:513>, <ore:plateAluminum>], [<ore:ingotSteel>, <ore:plateAluminum>, <ore:ingotSteel>]]);