mods.mekanism.infuser.addRecipe("REDSTONE", 80, <thermalfoundation:material:356>, <galacticraftcore:basic_item:14>);

mods.mekanism.infuser.addRecipe("REDSTONE", 80, <thermalfoundation:material:321>, <galacticraftcore:basic_item:13>);

mods.mekanism.infuser.addRecipe("CARBON", 80, <immersiveengineering:material:6>, <extraplanets:tier4_items:5>);

mods.mekanism.infuser.addRecipe("CARBON", 80, <thermalfoundation:material:802>, <extraplanets:tier4_items:5>);

mods.GalacticraftTweaker.removeCompressorRecipe(<extraplanets:tier4_items:3>);

mods.GalacticraftTweaker.removeCircuitFabricatorRecipe(<galacticraftcore:basic_item:13>);

mods.GalacticraftTweaker.removeCircuitFabricatorRecipe(<galacticraftcore:basic_item:14>);

mods.GalacticraftTweaker.removeCircuitFabricatorRecipe(<extraplanets:tier6_items:3>);

mods.GalacticraftTweaker.addCompressorShapelessRecipe(<extraplanets:tier4_items:3>, <extraplanets:tier4_items:4>, <thermalfoundation:material:357>, <galacticraftcore:heavy_plating>, null, null, null, null, null, null);

mods.GalacticraftTweaker.addCompressorShapelessRecipe(<extraplanets:tier6_items:3>, <extraplanets:tier4_items:4>, <thermalfoundation:material:328>, <thermalfoundation:material:328>, <thermalfoundation:material:328>, <galacticraftcore:heavy_plating>, null, null, null, null);